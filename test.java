package Mashkanta;

import static Mashkanta.PaymentCalculation.PaymentType;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import static Mashkanta.PaymentCalculation.LoanTrack;

public class test {
	
//	public static ArrayList<ArrayList<ArrayList<Integer>>> combArray = new ArrayList<ArrayList<ArrayList<Integer>>>();
//	public static ArrayList<ArrayList<Integer>> monthsComb = new ArrayList<ArrayList<Integer>>();
//	public static String s;
//	
//	static void createCombinationArray(ArrayList<Integer> numbers, int n, ArrayList<Integer> start) {
//		if (start.size() >= n) {
//			monthsComb.add(new ArrayList<>(start));
//		} else {
//			for (Integer x : numbers) {
//				start.add(x);
//				createCombinationArray(numbers, n, start);
//				start.remove(start.lastIndexOf(x));
//			}
//		}
//	}
	
	public static void main(String args[]) throws Exception {
//		double ob = 1000000.0;
		Database db = new Database();
		System.out.println(db.getIndexedAGAH().size());
//		LPAlgorithm lp = new LPAlgorithm(652000.0, 12 * 10, 4.15, db, 0.0, 0, true);
//		lp.LPcalc();
//		System.out.println("total amount=" + lp.bestRes.getTotalPayment());
		
//		ArrayList<ArrayList<ArrayList<Integer>>> list = new ArrayList<ArrayList<ArrayList<Integer>>>(25);
		
//		FileOutputStream fos = new FileOutputStream("combFile5");
//		ObjectOutputStream oos = new ObjectOutputStream(fos);
//		Integer first = 5;
//		ArrayList<Integer> num = new ArrayList<>();
//		num.clear();
//		num.add(first);
//		num.add(first-1);
//		num.add(first+1);
//		monthsComb.clear();
//		createCombinationArray(num, 14, new ArrayList<Integer>());
//		oos.writeObject(new ArrayList<ArrayList<Integer>>(monthsComb));
//		oos.close();
//		
//		fos = new FileOutputStream("combFile6");
//		oos = new ObjectOutputStream(fos);
//		first = 6;
//		num = new ArrayList<>();
//		num.clear();
//		num.add(first);
//		num.add(first-2);
//		num.add(first+2);
//		monthsComb.clear();
//		createCombinationArray(num, 14, new ArrayList<Integer>());
//		oos.writeObject(new ArrayList<ArrayList<Integer>>(monthsComb));
//		oos.close();
//		
//		for(int i=7; i<=30; i++){
//			fos = new FileOutputStream("combFile" + i);
//			oos = new ObjectOutputStream(fos);
//			first = i;
//			num = new ArrayList<>();
//			num.clear();
//			if(i < 28){
//				num.add(first);
//				num.add(first-3);
//				num.add(first+3);
//			}else if(i == 28){
//				num.add(first);
//				num.add(first-2);
//				num.add(first+2);
//			}else if(i == 29){
//				num.add(first);
//				num.add(first-2);
//				num.add(first+1);
//			}else{
//				num.add(first);
//				num.add(first-1);
//				num.add(first-2);
//			}
//			monthsComb.clear();
//			createCombinationArray(num, 14, new ArrayList<Integer>());
//			oos.writeObject(new ArrayList<ArrayList<Integer>>(monthsComb));
//			oos.close();
//		}
		
//		for(int i=1; i<=22; i++){
//			ArrayList<ArrayList<Integer>> newMonthsComb = new ArrayList<ArrayList<Integer>>();
//			Iterator<ArrayList<Integer>> monthsCombItr = monthsComb.iterator();
//			while(monthsCombItr.hasNext()){
//				ArrayList<Integer> newMonthsCombList = new ArrayList<Integer>();
//				ArrayList<Integer> monthsCombList = monthsCombItr.next();
//				Iterator<Integer> itr = monthsCombList.iterator();
//				while(itr.hasNext()){
//					Integer num1 = itr.next();
//					newMonthsCombList.add(num1+i);
//				}
//				newMonthsComb.add(new ArrayList<Integer>(newMonthsCombList));
//				newMonthsCombList.clear();
//			}
//			oos.writeObject(new ArrayList<ArrayList<Integer>>(newMonthsComb));
//			newMonthsComb.clear();
//		}
		
//		long start = System.currentTimeMillis();
//		FileInputStream fis = new FileInputStream("combFile28");
//		ObjectInputStream ois = new ObjectInputStream(fis);
//		ArrayList<ArrayList<Integer>> years = null;
////		for(int i=0; i<=22; i++)
//			years = (ArrayList<ArrayList<Integer>>) ois.readObject();
//			 s = years.toString();
//		System.out.println(s);
//		ois.close();
//		long end = System.currentTimeMillis();
//		System.out.println("It took : " + (end-start));


		// PaymentCalculation check
		
//		PaymentCalculation pc = new PaymentCalculation(new Database(), 700000.0, 10 * 12, PaymentType.SPITZER, LoanTrack.DOLLAR,10./12);
//		pc.Calculate();
//		PaymentCalculation.printHeader();
//		Iterator<MonthlyPayment> itr = pc.payment.iterator();
//		MonthlyPayment monthly = null;
//		int i = 1;
//		while (itr.hasNext()) {
//			monthly = itr.next();
//			Double t = monthly.getBalance();
//			PaymentCalculation.printSch(i, t, monthly.getMonthlyTotal(), monthly.getMonthlyInterest(), monthly.getMonthlyPrincipal(),
//					monthly.getBalance() - monthly.getMonthlyPrincipal());
//			i++;
//		}
//
//		System.out.println("Total paid: " + pc.getTotalPayment());

		// printAll check
		// StopWatch timer = new StopWatch();
		// timer.start();
		// Integer i[] = new Integer[] { 0, 5, 6 };
		// printAll(i, 13, new ArrayList<Integer>());
		// timer.stop();
		// System.out.println(timer.getNanoTime());
	}

	// public static void main(String args[]) throws IOException{
	// Database db = new Database();
	// List<Double> cpi = db.getCPI();
	// for(int i=0; i<cpi.size(); i++){
	// System.out.format("%d %-3.3f\n",i, cpi.get(i));
	// }
	//
	// }
	
	
	
//	private List<Double> calcCPIExpected() throws IOException {
//		List<Double> CPIPred = new ArrayList<Double>();
//		List<Double> CPI = new ArrayList<Double>();
//		
//		// E-2:  CPI - IMPLIED AND ADJUSTED INDICES							
//		// HSSFWorkbook workbook = readExcel(
//		//		"http://www.boi.org.il/en/DataAndStatistics/Lists/BoiTablesAndGraphs/eng_e02.xls");
//		
//		// Rate of Inflation 					
//		HSSFWorkbook workbook = readExcel(
//				"http://www.boi.org.il/en/DataAndStatistics/Lists/BoiTablesAndGraphs/shcf10_e.xls");
//
//		// Get first/desired sheet from the workbook
//		HSSFSheet sheet = workbook.getSheetAt(1);
//		Row row = sheet.getRow(9);
//		Iterator<Cell> cellIterator = row.iterator();
//		Cell cell = cellIterator.next();
//		// Iterate through each cell one by one
//		while (cellIterator.hasNext()) {
//			cell = cellIterator.next();
//			// Check the cell type and format accordingly
//			if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
//				CPIPred.add(cell.getNumericCellValue());
//			}
//		}
//		
//		CPI.add(0.0);
//		CPI.add(CPIPred.get(0)/11);
//		for(int i=2; i<(MAX_YEARS*12); i++){
//			if(i >= 2 && i <= 11){
//				CPI.add(CPI.get(i-1) + CPIPred.get(0)/11);
//			}else if(i >= 12 && i <= 23){
//				CPI.add(CPI.get(i-1) + (CPIPred.get(1)-CPIPred.get(0))/(23-12+1));
//			}else if(i >= 24 && i <= 59){
//				CPI.add(CPI.get(i-1) + (CPIPred.get(2)-CPIPred.get(1))/(59-24+1));
//			}else if(i >= 60 && i <= 119){
//				CPI.add(CPI.get(i-1) + (CPIPred.get(3)-CPIPred.get(2))/(119-60+1));
//			}else{
//				CPI.add(CPI.get(i-1) + (CPIPred.get(3)-CPIPred.get(2))/(119-60+1));
//			}
//		}
//		return CPI;
//	}
}
