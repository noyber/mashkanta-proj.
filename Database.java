package Mashkanta;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
//import com.opencsv.CSVReader;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Database {
	
	private List<Double> dollarPred = new ArrayList<Double>();
	private List<Double> euroPred = new ArrayList<Double>();
	private List<Double> dollarLiborPred = new ArrayList<Double>();
	private List<Double> euroLiborPred = new ArrayList<Double>();
	private List<Double> primePred = new ArrayList<Double>();
	private List<Double> CPIPred = new ArrayList<Double>();
	private List<Double> CPIIndexedMortgagesInterest = new ArrayList<Double>();
	private List<Double> indexedAGAHPred = new ArrayList<Double>();
	private List<Double> indexedAGAHFiveYearPred = new ArrayList<Double>();
	private List<Double> AGAHNotIndexedFiveYearPred = new ArrayList<Double>();
	final static int MAX_YEARS = 30;
	
	public Database() throws IOException{
		
		indexedAGAHPred = calcIndexedAGAH();
		AGAHNotIndexedFiveYearPred = calcAGAHFiveYears("http://www.boi.org.il/he/DataAndStatistics/Lists/BoiTablesAndGraphs/shcd08_h.xls");
		indexedAGAHFiveYearPred = calcAGAHFiveYears("http://www.boi.org.il/he/DataAndStatistics/Lists/BoiTablesAndGraphs/shcd07_h.xls");
		CPIIndexedMortgagesInterest = calcCPIIndexedMortgInterest();

		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormatBoi = new SimpleDateFormat("dd/MM/yyyy"); 
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, -10);
		cal.add(Calendar.MONTH, -1);
		Date tenYearsAgo = cal.getTime();

		URL CPIDb = new URL("http://www.boi.org.il/en/DataAndStatistics/Pages/boi.ashx?Command=DownloadSeriesExcel&SeriesCode=CP&DateStart=" + dateFormatBoi.format(tenYearsAgo) + "&DateEnd=" + dateFormatBoi.format(today) + "&Level=3&Sid=13");
		CPIPred = getSeriesData(CPIDb);
		
		URL dollarDb = new URL("http://www.boi.org.il/en/DataAndStatistics/Pages/boi.ashx?Command=DownloadSeriesExcel&SeriesCode=MAT01.MA&DateStart=" + dateFormatBoi.format(tenYearsAgo) + "&DateEnd=" + dateFormatBoi.format(today) + "&Level=4&Sid=50");
		dollarPred = getSeriesData(dollarDb);
		
		URL dollarLibor = new URL("http://www.boi.org.il/he/DataAndStatistics/Pages/boi.ashx?Command=DownloadSeriesExcel&SeriesCode=LIBOR_01.M_AV_M3&DateStart=" + dateFormatBoi.format(tenYearsAgo) + "&DateEnd=" + dateFormatBoi.format(today) + "&Level=5&Sid=12");
		dollarLiborPred = getSeriesData(dollarLibor);
		
		URL euroDb = new URL("http://www.boi.org.il/en/DataAndStatistics/Pages/boi.ashx?Command=DownloadSeriesExcel&SeriesCode=MAT27.M&DateStart=" + dateFormatBoi.format(tenYearsAgo) + "&DateEnd=" + dateFormatBoi.format(today) + "&Level=4&Sid=50");
		euroPred = getSeriesData(euroDb);
		
		// No database for euro LIBOR - using dollar database instead.
//		URL euroLiborDb = new URL("https://research.stlouisfed.org/fred2/graph/fredgraph.xls?chart_type=line&recession_bars=off&log_scales=&bgcolor=%23e1e9f0&graph_bgcolor=%23ffffff&fo=Open+Sans&ts=12&tts=12&txtcolor=%23444444&show_legend=yes&show_axis_titles=yes&drp=0&cosd=" + dateFormatFred.format(thirtyYearsAgo) + "&coed=2016-05-05&height=445&stacking=&range=Custom&mode=fred&id=EUR1MTD156N&transformation=lin&nd=&ost=-99999&oet=99999&lsv=&lev=&scale=left&line_color=%234572a7&line_style=solid&lw=2&mark_type=none&mw=2&mma=0&fml=a&fgst=lin&fgsnd=" + dateFormatFred.format(today) + "&fq=Monthly&fam=eop&vintage_date=&revision_date=&width=670");
//		euroLiborPred = getSeriesData(euroLiborDb);
		euroLiborPred = getSeriesData(dollarLibor);
		
		URL primeDb = new URL("http://www.boi.org.il/he/DataAndStatistics/Pages/boi.ashx?Command=DownloadSeriesExcel&SeriesCode=RIB_BID_AVNEZ.M&DateStart=" + dateFormatBoi.format(tenYearsAgo) + "&DateEnd=" + dateFormatBoi.format(today) + "&Level=3&Sid=22");
		primePred = getSeriesData(primeDb);
	}
	
	public Database(String fileString) throws IOException{
		FileInputStream fileInputStream = new FileInputStream(fileString);
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		
		// Get first/desired sheet from the workbook
		HSSFSheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.iterator();
		rowIterator.next();
		while(rowIterator.hasNext()){
			Row row = rowIterator.next();
			primePred.add(row.getCell(0).getNumericCellValue());
			CPIPred.add(row.getCell(1).getNumericCellValue());
			dollarPred.add(row.getCell(2).getNumericCellValue());
			dollarLiborPred.add(row.getCell(3).getNumericCellValue());
			euroPred.add(row.getCell(4).getNumericCellValue());
			euroLiborPred.add(row.getCell(5).getNumericCellValue());
			indexedAGAHPred.add(row.getCell(6).getNumericCellValue());
			indexedAGAHFiveYearPred.add(row.getCell(7).getNumericCellValue());
			AGAHNotIndexedFiveYearPred.add(row.getCell(8).getNumericCellValue());
			CPIIndexedMortgagesInterest.add(row.getCell(9).getNumericCellValue());
		}
		
		workbook.close();
	}
	
	/*private void readCSV(String address) throws IOException {
		URL url = new URL(address);
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		CSVReader reader = new CSVReader(in);
		// Read all rows at once
		List<String[]> allRows = reader.readAll();

		// Read CSV line by line and use the string array as you want

	}*/

	private HSSFWorkbook readExcel(String address) throws IOException {
		URL url = new URL(address);

		// First set the default cookie manager.
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		// Create Workbook instance holding reference to .xls file
		InputStream ExcelFileToRead = url.openStream();

		return new HSSFWorkbook(ExcelFileToRead);
	}

	public static void exportTable(JTable table, JTable mixTable, File file, String totalPayment, String amount, String interest) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		HSSFWorkbook workbook = new HSSFWorkbook();
		
        TableModel model = table.getModel();
        HSSFSheet worksheet1 = workbook.createSheet("���� �������");

        // Write loan amount, total payment and fixed interest to the file.
		String[] headers = { "���� �������", "��-��� ������", "����� �����" };
		String[] params = {amount, totalPayment, interest};
		HSSFRow r0 = worksheet1.createRow(0), r1 = worksheet1.createRow(1);
		for(int i=0; i<headers.length; i++){
			Cell cell0 = r0.createCell(i);
			cell0.setCellValue(headers[i]);
			Cell cell1 = r1.createCell(i);
			cell1.setCellValue(params[i]);
		}
        
        // Write the table to the file.
		HSSFRow r = worksheet1.createRow(3);
        String groupExport = "";
        for (int i = 0; i < (model.getColumnCount()); i++) { // export from TableHeaders
        	worksheet1.setColumnWidth(i, 5000);
            groupExport = String.valueOf(model.getColumnName(i));
            Cell c = r.createCell(i);
            c.setCellValue(String.valueOf(groupExport));
        }
        for (int i = 0; i < model.getRowCount(); i++) {
        	HSSFRow row = worksheet1.createRow(i+4);
            for (int j = 0; j < (model.getColumnCount()); j++) {
                if (model.getValueAt(i, j) != null) {
                    groupExport = String.valueOf(model.getValueAt(i, j));
                    Cell c = row.createCell(j);
                    c.setCellValue(String.valueOf(groupExport));
                }
            }
        }
        
        model = mixTable.getModel();
        HSSFSheet worksheet2 = workbook.createSheet("����� �����");
    
        // Write the table to the file.
		r = worksheet2.createRow(0);
		worksheet2.setColumnWidth(0, 2500);
		worksheet2.setColumnWidth(1, 2500);
		worksheet2.setColumnWidth(2, 2500);
		worksheet2.setColumnWidth(3, 10000);
        groupExport = "";
        for (int i = 0; i < (model.getColumnCount()); i++) { // export from TableHeaders
            groupExport = String.valueOf(model.getColumnName(i));
            Cell c = r.createCell(i);
            c.setCellValue(String.valueOf(groupExport));
        }
        for (int i = 0; i < model.getRowCount(); i++) {
        	HSSFRow row = worksheet2.createRow(i+1);
            for (int j = 0; j < (model.getColumnCount()); j++) {
                if (model.getValueAt(i, j) != null) {
                    groupExport = String.valueOf(model.getValueAt(i, j));
                    Cell c = row.createCell(j);
                    c.setCellValue(String.valueOf(groupExport));
                }
            }
        }
        
        workbook.write(out);
        workbook.close();
        out.flush();
        out.close();
    }
	
	public void writeTemplateFile(File file) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(file);
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet worksheet = workbook.createSheet("������ �����");

		String[] headers = {"����� ������", "��� ������� �����", "��� �����", "���� - ����� ������", "��� �����", "���� - ����� ������", "���� ��� ���� (�����)", "���� ��� ���� (5 ����)", "���� ������ ��� �������� ������ ����� ��� ���� (5 ����)", "���� �������� ������ ���"};
		
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
	    font.setFontHeightInPoints((short)24);
	    font.setFontName("Tahoma");
	    style.setFont(font);
	    HSSFRow row = worksheet.createRow(0);
		
		for(int i=0; i<headers.length; i++){
			worksheet.setColumnWidth(i, 6000);
			HSSFCell cell = row.createCell(i);
			cell.setCellValue(headers[i]);
		}
		
		for(int i=0; i<MAX_YEARS*12; i++){
			Row r = worksheet.createRow(i+1);
			r.createCell(0).setCellValue(primePred.get(i));
			r.createCell(1).setCellValue(CPIPred.get(i));
			r.createCell(2).setCellValue(dollarPred.get(i));
			r.createCell(3).setCellValue(dollarLiborPred.get(i));
			r.createCell(4).setCellValue(euroPred.get(i));
			r.createCell(5).setCellValue(euroLiborPred.get(i));
			r.createCell(6).setCellValue(indexedAGAHPred.get(i));
			r.createCell(7).setCellValue(indexedAGAHFiveYearPred.get(i));
			r.createCell(8).setCellValue(AGAHNotIndexedFiveYearPred.get(i));
			r.createCell(9).setCellValue(CPIIndexedMortgagesInterest.get(i));
		}
		
        workbook.write(fileOut);
        workbook.close();
        fileOut.flush();
		fileOut.close();
    }
	
	private List<Double> calcCPIIndexedMortgInterest() throws IOException{
		List<Double> rate = new ArrayList<Double>();
		HSSFWorkbook workbook = readExcel(
				"http://www.boi.org.il/he/BankingSupervision/Data/Documents/pribmash.xls");

		// Get first/desired sheet from the workbook
		// RESULT sheet
		HSSFSheet sheet = workbook.getSheetAt(2);
		Iterator<Row> rowIterator = sheet.iterator();
		while(rowIterator.hasNext()){
			Row r = rowIterator.next();
			if(r.getCell(0).getStringCellValue().equals("Average")){
				break;
			}
		}
		while(rowIterator.hasNext()){
			Cell cell = rowIterator.next().getCell(0);
			if(cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
				rate.add(cell.getNumericCellValue());
			}
		}
		
		// RESULT_OLD sheet
		sheet = workbook.getSheetAt(3);
		rowIterator = sheet.iterator();
		while(rowIterator.hasNext()){
			Row r = rowIterator.next();
			if(r.getCell(0).getStringCellValue().equals("Average")){
				break;
			}
		}
		while(rowIterator.hasNext()){
			Row r = rowIterator.next();
			// Get the date field, we want to avoid mid-month dates (We have them from 2008 and earlier).
			if(r.getCell(8).getCellType() == Cell.CELL_TYPE_NUMERIC){
				Date date = r.getCell(8).getDateCellValue();
				Calendar cal = Calendar.getInstance();
			    cal.setTime(date);
				if(cal.get(Calendar.DAY_OF_MONTH) == 15){
					continue;
				}
			}
			
			Cell cell = r.getCell(0);
			if(cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
				rate.add(cell.getNumericCellValue());
			}
		}
		
		// Take only the last 10 years and extend the array to 30 years.
		rate = rate.subList(0, 12*10);
		Collections.reverse(rate);
		rate.addAll(rate.subList(0, 12*10));
		rate.addAll(rate.subList(0, 12*10));
		return rate;
	}
	
	private List<Double> calcIndexedAGAH() throws IOException{
		List<Double> agah = new ArrayList<Double>();
		HSSFWorkbook workbook = readExcel("http://www.boi.org.il/he/DataAndStatistics/Lists/BoiTablesAndGraphs/shcd07_h.xls");
		// Get first/desired sheet from the workbook
		HSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		while(rowIterator.hasNext()){
			Row r = rowIterator.next();
			if(r.getCell(5).getNumericCellValue()==3 && r.getCell(6).getNumericCellValue()==4 && r.getCell(7).getNumericCellValue()==5){
				break;
			}
		}
		while(rowIterator.hasNext()){
			Row row = rowIterator.next();
			if(row.getCell(2) != null && row.getCell(2).getCellType() == Cell.CELL_TYPE_STRING && row.getCell(2).getStringCellValue().equals("������") &&
					row.getCell(5) != null && row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC && 
					row.getCell(6) != null && row.getCell(6).getCellType() == Cell.CELL_TYPE_NUMERIC && 
					row.getCell(7) != null && row.getCell(7).getCellType() == Cell.CELL_TYPE_NUMERIC){
				Double threeYears = row.getCell(5).getNumericCellValue(),
						fourYears = row.getCell(6).getNumericCellValue(),
						fiveYears = row.getCell(7).getNumericCellValue();
				agah.add((threeYears+fourYears+fiveYears)/3.);
			}
		}
		// We have only 8.33 years, need to extend
		agah.addAll(agah);
		agah.addAll(agah);
		// Take only 30 years
		agah = agah.subList(0, 12*MAX_YEARS);
		return agah;
	}
	
	private List<Double> calcAGAHFiveYears(String url) throws IOException{
		List<Double> agah = new ArrayList<Double>();
		HSSFWorkbook workbook = readExcel(url);
		// Get first/desired sheet from the workbook
		HSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		while(rowIterator.hasNext()){
			Row r = rowIterator.next();
			if(r.getCell(7).getNumericCellValue()==5){
				break;
			}
		}
		while(rowIterator.hasNext()){
			Row row = rowIterator.next();
			if(row.getCell(2) != null && row.getCell(2).getCellType() == Cell.CELL_TYPE_STRING && row.getCell(2).getStringCellValue().equals("������") &&
					row.getCell(7) != null && row.getCell(7).getCellType() == Cell.CELL_TYPE_NUMERIC){
				Double fiveYears = row.getCell(7).getNumericCellValue();
				agah.add(fiveYears);
			}
		}
		// We have only 8.33 years, need to extend
		agah.addAll(agah);
		agah.addAll(agah);
		// Take only 30 years
		agah = agah.subList(0, 12*MAX_YEARS);
		return agah;
	}
 	
	private List<Double> getSeriesData(URL exchangeURL) throws IOException {
		List<Double> data = new ArrayList<Double>();

        BufferedReader in = new BufferedReader(new InputStreamReader(exchangeURL.openStream()));
        
        String inputLine;
        // Skip all headers
        while ((inputLine = in.readLine()) != null){
        	String l = inputLine.replaceAll("\\<.*?>", "").trim();
        	if(l.startsWith("1") || l.startsWith("2") || l.startsWith("3")){
        		String[] lSplitted = l.split("\\s+");
        		data.add(Double.parseDouble(lSplitted[1]));
        	}
        }
        in.close();
        
        data.addAll(data);
        data.addAll(data.subList(0, 10*12));
		return data;
	}
	
	private List<Double> calcMonthlyChange(List<Double> list, Integer months){
		List<Double> newList = new ArrayList<Double>();
		Integer i = months;
        Double was = list.get(0);
        while(i < list.size()){
        	Double now = list.get(i);
        	for(int j=0; j<months; j++){
        		newList.add(now/was);
            }
        	was = now;
        	i+=months;
        }
		return newList;
	}
	
	private List<Double> calcMonthly(List<Double> list, Integer months){
		List<Double> newList = new ArrayList<Double>();
		Integer i = 0;
        while(i < list.size()){
        	Double now = list.get(i);
        	for(int j=0; j<months; j++){
        		newList.add(now);
            }
        	i+=months;
        }
		return newList;
	}
	
	public List<Double> getPrime(){
		List<Double> list = primePred;
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getCPI(){
		return CPIPred;
	}
	
	public List<Double> getDollar(){
		return calcMonthlyChange(dollarPred, 1);
	}
	
	public List<Double> getDollarLibor(){
		List<Double> list = calcMonthly(dollarLiborPred, 6);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getEuro(){
		return calcMonthlyChange(euroPred, 1);
	}
	
	public List<Double> getEuroLibor(){
		List<Double> list = calcMonthly(euroLiborPred, 6);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getIndexedAGAH(){
		List<Double> list = indexedAGAHPred;
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getIndexedAGAHOneYear(){
		List<Double> list = calcMonthly(indexedAGAHPred, 12);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getIndexedAGAHTwoYear(){
		List<Double> list = calcMonthly(indexedAGAHPred, 12*2);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}

	public List<Double> getindexedAGAHFiveYearPred(){
		List<Double> list = calcMonthly(indexedAGAHFiveYearPred, 12*5);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getAGAHNotIndexedFiveYearPred(){
		List<Double> list = calcMonthly(AGAHNotIndexedFiveYearPred, 12*5);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getCPIIndexedMortgagesInterest(){
		List<Double> list = CPIIndexedMortgagesInterest;
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getCPIIndexedMortgagesInterestOneYear(){
		List<Double> list = calcMonthly(CPIIndexedMortgagesInterest, 12);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getCPIIndexedMortgagesInterestTwoYear(){
		List<Double> list = calcMonthly(CPIIndexedMortgagesInterest, 12*2);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
	
	public List<Double> getCPIIndexedMortgagesInterestFiveYear(){
		List<Double> list = calcMonthly(CPIIndexedMortgagesInterest, 12*5);
		for(Integer i=0; i<list.size(); i++){
			if(list.get(i) > 10e-9){
				list.set(i, list.get(i)/12);
			}else{
				list.set(i, 10e-9);
			}
		}
		return list;
	}
}
