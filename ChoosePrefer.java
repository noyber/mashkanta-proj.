package Mashkanta;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.FontUIResource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Graphics;

public class ChoosePrefer extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Double prefered[] = new Double[] {1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.};

	/**
	 * Create the frame.
	 */
	public ChoosePrefer() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 356);
		contentPane = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
			    super.paintComponent(g); // paint the background image and scale it to fill the entire space
			    BufferedImage image = null;
				try {
					image = ImageIO.read(this.getClass().getResource("/pile1.jpg"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    g.drawImage(image, 0, 0, null);
			}
		};
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		UIManager.put("CheckBox.font", new FontUIResource("Tahoma", Font.PLAIN, 11));
		
		JCheckBox primeCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05E2\u05DC \u05D1\u05E1\u05D9\u05E1 \u05D4\u05E4\u05E8\u05D9\u05D9\u05DD");
		primeCheckBox.setSelected(true);
		primeCheckBox.setOpaque(false);
		primeCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		
		JCheckBox dollarCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05E2\u05DC \u05D1\u05E1\u05D9\u05E1 \u05D3\u05D5\u05DC\u05E8 \u05D0\u05E8\u05D4\"\u05D1");
		dollarCheckBox.setSelected(true);
		dollarCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		dollarCheckBox.setOpaque(false);
		
		JCheckBox euroCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05E2\u05DC \u05D1\u05E1\u05D9\u05E1 \u05D0\u05D9\u05E8\u05D5");
		euroCheckBox.setSelected(true);
		euroCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		euroCheckBox.setOpaque(false);
		
		JCheckBox oneYearMadadOgenCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC \u05E9\u05E0\u05D4 \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E8\u05D9\u05D1\u05D9\u05EA \u05E2\u05D5\u05D2\u05DF)");
		oneYearMadadOgenCheckBox.setSelected(true);
		oneYearMadadOgenCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		oneYearMadadOgenCheckBox.setOpaque(false);
		
		JCheckBox twoYearMadadOgenCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC \u05E9\u05E0\u05EA\u05D9\u05D9\u05DD \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E8\u05D9\u05D1\u05D9\u05EA \u05E2\u05D5\u05D2\u05DF)");
		twoYearMadadOgenCheckBox.setSelected(true);
		twoYearMadadOgenCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		twoYearMadadOgenCheckBox.setOpaque(false);
		
		JCheckBox fiveYearMadadOgenCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC 5 \u05E9\u05E0\u05D9\u05DD \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E8\u05D9\u05D1\u05D9\u05EA \u05E2\u05D5\u05D2\u05DF)");
		fiveYearMadadOgenCheckBox.setSelected(true);
		fiveYearMadadOgenCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		fiveYearMadadOgenCheckBox.setOpaque(false);
		
		JCheckBox fiveYearNoMadadCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC 5 \u05E9\u05E0\u05D9\u05DD, \u05DC\u05D0 \u05E6\u05DE\u05D5\u05D3 \u05DC\u05DE\u05D3\u05D3");
		fiveYearNoMadadCheckBox.setSelected(true);
		fiveYearNoMadadCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		fiveYearNoMadadCheckBox.setOpaque(false);
		
		JCheckBox constMadadCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05E7\u05D1\u05D5\u05E2\u05D4 \u05E6\u05DE\u05D5\u05D3\u05D4 \u05DC\u05DE\u05D3\u05D3");
		constMadadCheckBox.setSelected(true);
		constMadadCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		constMadadCheckBox.setOpaque(false);
		
		JCheckBox constNoMadadCheckBox = new JCheckBox("\u05E8\u05D9\u05D1\u05D9\u05EA \u05E7\u05D1\u05D5\u05E2\u05D4 \u05DC\u05D0 \u05E6\u05DE\u05D5\u05D3\u05D4 \u05DC\u05DE\u05D3\u05D3 ");
		constNoMadadCheckBox.setSelected(true);
		constNoMadadCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		constNoMadadCheckBox.setOpaque(false);
		
		JLabel label = new JLabel("\u05D1\u05D7\u05E8 \u05D0\u05EA \u05D4\u05DE\u05E1\u05DC\u05D5\u05DC\u05D9\u05DD \u05D4\u05DE\u05D5\u05E2\u05D3\u05E4\u05D9\u05DD \u05E2\u05DC\u05D9\u05DA:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setOpaque(false);
		
		JCheckBox oneYearMadadAGAHCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC \u05E9\u05E0\u05D4 \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E2\u05D5\u05D2\u05DF \u05D0\u05D2\"\u05D7 \u05E6\u05DE\u05D5\u05D3)");
		oneYearMadadAGAHCheckBox.setSelected(true);
		oneYearMadadAGAHCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		oneYearMadadAGAHCheckBox.setOpaque(false);
		
		JCheckBox twoYearMadadAGAHCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC \u05E9\u05E0\u05EA\u05D9\u05D9\u05DD \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E2\u05D5\u05D2\u05DF \u05D0\u05D2\"\u05D7 \u05E6\u05DE\u05D5\u05D3)");
		twoYearMadadAGAHCheckBox.setSelected(true);
		twoYearMadadAGAHCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		twoYearMadadAGAHCheckBox.setOpaque(false);
		
		JCheckBox fiveYearMadadAGAHCheckBox = new JCheckBox("\u05DE\u05E1\u05DC\u05D5\u05DC \u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05EA\u05E0\u05D4 \u05DB\u05DC 5 \u05E9\u05E0\u05D9\u05DD \u05E6\u05DE\u05D5\u05D3\u05EA \u05DE\u05D3\u05D3 (\u05E2\u05D5\u05D2\u05DF \u05D0\u05D2\"\u05D7 \u05E6\u05DE\u05D5\u05D3)");
		fiveYearMadadAGAHCheckBox.setSelected(true);
		fiveYearMadadAGAHCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		fiveYearMadadAGAHCheckBox.setOpaque(false);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(65, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
							.addComponent(constNoMadadCheckBox)
							.addComponent(constMadadCheckBox)
							.addComponent(fiveYearNoMadadCheckBox)
							.addComponent(fiveYearMadadOgenCheckBox)
							.addComponent(twoYearMadadOgenCheckBox)
							.addComponent(primeCheckBox)
							.addComponent(dollarCheckBox)
							.addComponent(euroCheckBox)
							.addComponent(oneYearMadadOgenCheckBox)
							.addComponent(oneYearMadadAGAHCheckBox)
							.addComponent(twoYearMadadAGAHCheckBox)
							.addComponent(fiveYearMadadAGAHCheckBox))
						.addComponent(label, Alignment.TRAILING))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(7)
					.addComponent(primeCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(dollarCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(euroCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(oneYearMadadOgenCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(oneYearMadadAGAHCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(twoYearMadadOgenCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(twoYearMadadAGAHCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(fiveYearMadadOgenCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(fiveYearMadadAGAHCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(fiveYearNoMadadCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(constMadadCheckBox)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(constNoMadadCheckBox)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		
		primeCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(primeCheckBox.isSelected()){
					prefered[0] = 1.;
					prefered[1] = 1.;
					prefered[2] = 1.;
				}else{
					prefered[0] = 0.;
					prefered[1] = 0.;
					prefered[2] = 0.;
				}
			}
		});
		dollarCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(dollarCheckBox.isSelected()){
					prefered[3] = 1.;
					prefered[4] = 1.;
					prefered[5] = 1.;
				}else{
					prefered[3] = 0.;
					prefered[4] = 0.;
					prefered[5] = 0.;
				}
			}
		});
		euroCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(euroCheckBox.isSelected()){
					prefered[6] = 1.;
					prefered[7] = 1.;
					prefered[8] = 1.;
				}else{
					prefered[6] = 0.;
					prefered[7] = 0.;
					prefered[8] = 0.;
				}
			}
		});
		oneYearMadadOgenCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(oneYearMadadOgenCheckBox.isSelected()){
					prefered[9] = 1.;
					prefered[10] = 1.;
				}else{
					prefered[9] = 0.;
					prefered[10] = 0.;
				}
			}
		});
		oneYearMadadAGAHCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(oneYearMadadAGAHCheckBox.isSelected()){
					prefered[11] = 1.;
					prefered[12] = 1.;
				}else{
					prefered[11] = 0.;
					prefered[12] = 0.;
				}
			}
		});
		twoYearMadadOgenCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(twoYearMadadOgenCheckBox.isSelected()){
					prefered[13] = 1.;
				}else{
					prefered[13] = 0.;
				}
			}
		});
		twoYearMadadAGAHCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(twoYearMadadAGAHCheckBox.isSelected()){
					prefered[14] = 1.;
				}else{
					prefered[14] = 0.;
				}
			}
		});
		fiveYearMadadOgenCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(fiveYearMadadOgenCheckBox.isSelected()){
					prefered[15] = 1.;
				}else{
					prefered[15] = 0.;
				}
			}
		});
		fiveYearMadadAGAHCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(fiveYearMadadAGAHCheckBox.isSelected()){
					prefered[16] = 1.;
				}else{
					prefered[16] = 0.;
				}
			}
		});
		fiveYearNoMadadCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(fiveYearNoMadadCheckBox.isSelected()){
					prefered[17] = 1.;
				}else{
					prefered[17] = 0.;
				}
			}
		});
		constMadadCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(constMadadCheckBox.isSelected()){
					prefered[18] = 1.;
					prefered[19] = 1.;
					prefered[20] = 1.;
				}else{
					prefered[18] = 0.;
					prefered[19] = 0.;
					prefered[20] = 0.;
				}
			}
		});
		constNoMadadCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(constNoMadadCheckBox.isSelected()){
					prefered[21] = 1.;
					prefered[22] = 1.;
					prefered[23] = 1.;
				}else{
					prefered[21] = 0.;
					prefered[22] = 0.;
					prefered[23] = 0.;
				}
			}
		});
	}
	
	public Double[] getPrefered(){
		return prefered;
	}
}
