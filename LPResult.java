package Mashkanta;
import java.util.ArrayList;
import java.util.Iterator;

public class LPResult {
	private Double totalAmount;
	private Double totalPayment;
	private Double interest;
	private ArrayList<Integer> monthsComb;
	private double[] sol;
	private PaymentCalculation pc;
	
	LPResult(ArrayList<Integer> monthsComb, double[] sol, PaymentCalculation pc, Double totalAmount, Double interest){
		this.totalPayment = pc.getTotalPayment();
		this.monthsComb = new ArrayList<Integer>(monthsComb);
		this.sol = sol;
		this.pc = pc;
		this.totalAmount = totalAmount;
		this.interest = interest;
	}
	
	LPResult(){
		this.totalPayment = Double.MAX_VALUE;
		monthsComb = new ArrayList<Integer>();
	}
	
	public boolean swapIfBetter(LPResult res){
		if(res.getTotalPayment() < this.getTotalPayment()){
			this.setTotalPayment(res.getTotalPayment());
			this.setTotalAmount(res.getTotalAmount());
			this.setMonthsComb(res.getMonthsComb());
			this.setSol(res.getSol());
			this.setPc(res.getPc());
			this.setInterest(res.getInterest());
			return true;
		}
		return false;
	}
	
	public void setTotalPayment(Double totalPayment){
		this.totalPayment = totalPayment;
	}
	
	public void setTotalAmount(Double totalAmount){
		this.totalAmount = totalAmount;
	}
	
	public void setMonthsComb(ArrayList<Integer> monthsComb){
		this.monthsComb = new ArrayList<Integer>(monthsComb);
	}
	
	public void setSol(double sol[]){
		this.sol = sol;
	}
	
	public void setPc(PaymentCalculation pc){
		this.pc = pc;
	}
	
	public void setInterest(Double interest){
		this.interest = interest;
	}
	
	public Double getTotalPayment(){
		return this.totalPayment;
	}
	
	public Double getTotalAmount(){
		return this.totalAmount;
	}
	
	public ArrayList<Integer> getMonthsComb(){
		return this.monthsComb;
	}
	
	public double[] getSol(){
		return this.sol;
	}
	
	public PaymentCalculation getPc(){
		return this.pc;
	}
	
	public Double getInterest(){
		return this.interest;
	}
	
	public Double getAvgPayment(){
		Double avg = 0.0;
		Integer i=0;
		Iterator<MonthlyPayment> itr = getPc().getPayments().iterator();
		while(itr.hasNext()){
			MonthlyPayment p = itr.next();
			avg+=p.getMonthlyTotal();
			i++;
		}
		avg/=i;
		return avg;
	}
}
