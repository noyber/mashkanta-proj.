package Mashkanta;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;
import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class GuiInterface {

	private JFrame frame;
	JFormattedTextField amountFormatted;
	JFormattedTextField yearsFormatted;
	JFormattedTextField rateFormatted;
	private JTextField chosenTextField;
	private JTextField bulletTextField;
	private JTextField timeTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiInterface window = new GuiInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public GuiInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {		
		frame = new JFrame();
		JLabel contentPane = new JLabel();
		ImageIcon image = new ImageIcon(this.getClass().getResource("/m4.jpg"));
		contentPane.setIcon(image);
//		contentPane.setLayout( new BorderLayout() );
		frame.setContentPane( contentPane );
		frame.setBounds(100, 100, 660, 645);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ChoosePrefer chooseFrame = new ChoosePrefer();

		UIManager.put("ToolTip.font", new FontUIResource("Tahoma", Font.PLAIN, 14));
		UIManager.put("OptionPane.messageFont", new FontUIResource("Tahoma", Font.PLAIN, 16));
		UIManager.put("OptionPane.buttonFont", new FontUIResource("Tahoma", Font.PLAIN, 16));
		ToolTipManager.sharedInstance().setDismissDelay(15000);

		JCheckBox openFileCheckBox = new JCheckBox(
				"\u05D9\u05D9\u05D1\u05D0 \u05E7\u05D5\u05D1\u05E5 \u05EA\u05D7\u05D6\u05D9\u05D5\u05EA");
		openFileCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		openFileCheckBox.setHorizontalAlignment(SwingConstants.RIGHT);
		openFileCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		openFileCheckBox.setOpaque(false);
		openFileCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (openFileCheckBox.isSelected()) {
					chosenTextField.setEnabled(true);
				} else {
					chosenTextField.setEnabled(false);
				}
			}
		});
		
		JCheckBox bulletCheckBox = new JCheckBox("\u05D1\u05D7\u05E8 \u05D1\u05E9\u05D9\u05D8\u05EA \u05D4\u05D7\u05D6\u05E8 \u05D1\u05D5\u05DC\u05D9\u05D8");
		bulletCheckBox.setToolTipText("<html>\u05D1\u05E9\u05D9\u05D8\u05D4 \u05D6\u05D5 \u05DE\u05E9\u05D5\u05DC\u05DE\u05EA, \u05DE\u05D3\u05D9 \u05D7\u05D5\u05D3\u05E9, \u05D4\u05E8\u05D9\u05D1\u05D9\u05EA \u05D1\u05DC\u05D1\u05D3. \u05D4\u05E8\u05D9\u05D1\u05D9\u05EA \u05DE\u05E9\u05D5\u05DC\u05DE\u05EA \u05E2\u05DC<br>\u05DE\u05DC\u05D5\u05D0 \u05E1\u05DB\u05D5\u05DD \u05D4\u05D4\u05DC\u05D5\u05D5\u05D0\u05D4 (\u05DB\u05D5\u05DC\u05DC \u05D4\u05E6\u05DE\u05D3\u05D4 \u05D1\u05DE\u05E1\u05DC\u05D5\u05DC\u05D9\u05DD \u05E6\u05DE\u05D5\u05D3\u05D9 \u05DE\u05D3\u05D3). \u05D1\u05E1\u05D9\u05D5\u05DD <br>\u05EA\u05E7\u05D5\u05E4\u05EA \u05D4\u05D4\u05DC\u05D5\u05D5\u05D0\u05D4 \u05D9\u05E9 \u05DC\u05E9\u05DC\u05DD \u05D0\u05EA \u05D4\u05E1\u05DB\u05D5\u05DD \u05D4\u05DE\u05E7\u05D5\u05E8\u05D9 \u05E9\u05DC \u05D4\u05E7\u05E8\u05DF \u05D1\u05EA\u05E9\u05DC\u05D5\u05DD \u05D0\u05D7\u05D3,<br>\u05DB\u05D5\u05DC\u05DC \u05D4\u05E6\u05DE\u05D3\u05D4 \u05E2\u05DC \u05E1\u05DB\u05D5\u05DD \u05D4\u05E7\u05E8\u05DF, \u05D1\u05D4\u05EA\u05D0\u05DD \u05DC\u05DE\u05E1\u05DC\u05D5\u05DC \u05D4\u05D4\u05DC\u05D5\u05D5\u05D0\u05D4 \u05E9\u05E0\u05DC\u05E7\u05D7. \u05E9\u05D9\u05D8\u05D4<br>\u05D6\u05D5 \u05DE\u05EA\u05D0\u05D9\u05DE\u05D4 \u05DC\u05D0\u05E0\u05E9\u05D9\u05DD \u05E9\u05E2\u05EA\u05D9\u05D3\u05D9\u05DD \u05DC\u05E7\u05D1\u05DC \u05E1\u05DB\u05D5\u05DD \u05DB\u05E1\u05E3 \u05DE\u05E1\u05D5\u05D9\u05DD \u05D1\u05E9\u05E0\u05D9\u05DD \u05D4\u05E7\u05E8\u05D5\u05D1\u05D5\u05EA.</html>");
		bulletCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		bulletCheckBox.setHorizontalAlignment(SwingConstants.RIGHT);
		bulletCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		bulletCheckBox.setOpaque(false);
		bulletCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(bulletCheckBox.isSelected()){
					bulletTextField.setEnabled(true);
				}else{
					bulletTextField.setEnabled(false);
				}
			}
		});
		
		JCheckBox yearCombCheckBox = new JCheckBox("\u05D1\u05D3\u05D5\u05E7 \u05E7\u05D5\u05DE\u05D1\u05D9\u05E0\u05E6\u05D9\u05D5\u05EA \u05E9\u05D5\u05E0\u05D5\u05EA \u05E9\u05DC \u05E9\u05E0\u05D9\u05DD");
		yearCombCheckBox.setOpaque(false);
		yearCombCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		yearCombCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		yearCombCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(yearCombCheckBox.isSelected()){
					timeTextField.setEnabled(true);
				}else{
					timeTextField.setEnabled(false);
				}
			}
		});
		
		JComboBox<Integer> bulletComboBox = new JComboBox<Integer>();
		bulletComboBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bulletComboBox.setModel(new DefaultComboBoxModel<Integer>(new Integer[] { 3, 4, 5 }));
		
		JButton calcButton = new JButton();
		calcButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		calcButton.setHorizontalTextPosition(SwingConstants.CENTER);
		calcButton.setText("\u05D7\u05E9\u05D1");
		calcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Double amount = Double.parseDouble(amountFormatted.getText().replaceAll(",", ""));
				Integer months = Integer.parseInt(yearsFormatted.getText()) * 12;
				Double rate = Double.parseDouble(rateFormatted.getText()) / 12.0;
				Double bulletAmount = Double.parseDouble(bulletTextField.getText());
				Double time = Double.parseDouble(timeTextField.getText());
				File file = new File(chosenTextField.getText());
				// Invalid Input
				if (amount == 0.0 || months == 0 || rate == 0.0 || (bulletCheckBox.isSelected() && bulletAmount == 0.0) || (openFileCheckBox.isSelected() && !file.exists()) || (yearCombCheckBox.isSelected() && time == 0.0)) {
					Object[] option = { "\u05D0\u05D9\u05E9\u05D5\u05E8" };
					JOptionPane.showOptionDialog(frame,
							"\u05D1\u05D1\u05E7\u05E9\u05D4 \u05D4\u05DB\u05E0\u05E1 \u05E2\u05E8\u05DB\u05D9\u05DD \u05D7\u05D5\u05E7\u05D9\u05D9\u05DD.",
							"\u05E9\u05D2\u05D9\u05D0\u05D4", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null,
							option, option[0]);
				// Input is OK, lets calculate!
				} else {
					try {
						Database db;
						if(openFileCheckBox.isSelected()){
							db = new Database(chosenTextField.getText());
						}else{
							db = new Database();
						}

						Integer timeout = Integer.valueOf(timeTextField.getText())*1000;
						Integer bulletYears = (Integer)bulletComboBox.getSelectedItem();
						LPAlgorithm lp;
						if(bulletCheckBox.isSelected()){
							lp = new LPAlgorithm(amount, months, rate, db, chooseFrame.getPrefered(), bulletAmount, bulletYears*12, yearCombCheckBox.isSelected(), timeout);
						}else{
							lp = new LPAlgorithm(amount, months, rate, db, chooseFrame.getPrefered(), 0.0, 0, yearCombCheckBox.isSelected(), timeout);
						}
						LPResult res = lp.getBestResult();
						
						GuiResult frame = new GuiResult(res);
						frame.setVisible(true);
					} catch (Exception e) {
						Object[] option = { "\u05D0\u05D9\u05E9\u05D5\u05E8" };
						JOptionPane.showOptionDialog(frame,
								"����� �����, ��� ���� ����� ����� ��������.",
								"\u05E9\u05D2\u05D9\u05D0\u05D4", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null,
								option, option[0]);
					}
				}
			}
		});
		calcButton.setToolTipText(
				"\u05D7\u05E9\u05D1 \u05D0\u05EA \u05D4\u05DE\u05E9\u05DB\u05E0\u05EA\u05D0 \u05D4\u05D0\u05D5\u05E4\u05D8\u05D9\u05DE\u05DC\u05D9\u05EA");
		calcButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		ImageIcon calculateIcon = new ImageIcon(this.getClass().getResource("/Calculator-52.png"));
		calcButton.setIcon(calculateIcon);

		JButton openFileButton = new JButton();
		openFileButton.setHorizontalTextPosition(SwingConstants.CENTER);
		openFileButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		openFileButton.setText("\u05D9\u05D9\u05D1\u05D0 \u05E7\u05D5\u05D1\u05E5");
		openFileButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		openFileButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					chosenTextField.setText(file.getAbsolutePath());
				} else {
					Object[] option = { "\u05D0\u05D9\u05E9\u05D5\u05E8" };
					JOptionPane.showOptionDialog(frame,
							"\u05D4\u05E4\u05E2\u05D5\u05DC\u05D4 \u05D1\u05D5\u05D8\u05DC\u05D4 \u05E2\u05DC-\u05D9\u05D3\u05D9 \u05D4\u05DE\u05E9\u05EA\u05DE\u05E9.",
							"\u05D1\u05D9\u05D8\u05D5\u05DC", JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION, null,
							option, option[0]);
				}
				System.out.println(returnVal);
			}
		});
		ImageIcon openFileIcon = new ImageIcon(this.getClass().getResource("/Add File-52.png"));
		openFileButton.setIcon(openFileIcon);

		amountFormatted = new JFormattedTextField();
		amountFormatted.setHorizontalAlignment(SwingConstants.CENTER);
		amountFormatted.setFont(new Font("Tahoma", Font.PLAIN, 16));
		amountFormatted.setText("0");

		yearsFormatted = new JFormattedTextField();
		yearsFormatted.setHorizontalAlignment(SwingConstants.CENTER);
		yearsFormatted.setFont(new Font("Tahoma", Font.PLAIN, 16));
		yearsFormatted.setText("0");

		JLabel amountLabel = new JLabel("\u05E1\u05DB\u05D5\u05DD \u05DE\u05E9\u05DB\u05E0\u05EA\u05D0");
		amountLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JLabel yearsLabel = new JLabel("\u05DE\u05E1\u05E4\u05E8 \u05E9\u05E0\u05D9\u05DD");
		yearsLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JLabel rateLabel = new JLabel("\u05E8\u05D9\u05D1\u05D9\u05EA \u05E9\u05E0\u05EA\u05D9\u05EA");
		rateLabel.setToolTipText("\u05D4\u05E8\u05D9\u05D1\u05D9\u05EA \u05D4\u05E7\u05D1\u05D5\u05E2\u05D4 \u05D4\u05E0\u05D9\u05EA\u05E0\u05EA \u05E2\"\u05D9 \u05D4\u05D1\u05E0\u05E7 \u05D4\u05DE\u05DC\u05D5\u05D5\u05D4.");
		rateLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		rateFormatted = new JFormattedTextField();
		rateFormatted.setText("0");
		rateFormatted.setHorizontalAlignment(SwingConstants.CENTER);
		rateFormatted.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JSlider amountSlider = new JSlider();
		amountSlider.setValue(0);
		amountSlider.setMaximum(5000000);
		amountSlider.setOpaque(false);
		amountSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				amountFormatted.setValue(amountSlider.getValue());
			}
		});

		JSlider yearSlider = new JSlider();
		yearSlider.setMaximum(30);
		yearSlider.setValue(0);
		yearSlider.setOpaque(false);
		yearSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				yearsFormatted.setValue(yearSlider.getValue());
			}
		});

		JSlider rateSlider = new JSlider();
		rateSlider.setMaximum(1000);
		rateSlider.setValue(0);
		rateSlider.setOpaque(false);
		rateSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				rateFormatted.setValue(rateSlider.getValue() / 100.0);
			}
		});

		frame.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				amountSlider.setValue(Integer.parseInt(amountFormatted.getText().replaceAll(",", "")));
				yearSlider.setValue(Integer.parseInt(yearsFormatted.getText()));
				rateSlider.setValue((int) (Double.parseDouble(rateFormatted.getText()) * 100));
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});

		chosenTextField = new JTextField();
		chosenTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		chosenTextField.setEnabled(false);
		chosenTextField.setColumns(10);
		chosenTextField.setToolTipText("\u05D4\u05E7\u05D5\u05D1\u05E5 \u05D4\u05E0\u05D1\u05D7\u05E8");

		bulletTextField = new JTextField();
		bulletTextField.setText("0");
		bulletTextField.setHorizontalAlignment(SwingConstants.CENTER);
		bulletTextField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bulletTextField.setEnabled(false);
		bulletTextField.setColumns(10);

		JLabel bulletAmountLabel = new JLabel("\u05E1\u05DB\u05D5\u05DD");
		bulletAmountLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel bulletYearLabel = new JLabel("\u05E9\u05E0\u05D9\u05DD");
		bulletYearLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel timeLabel = new JLabel("\u05D6\u05DE\u05DF");
		timeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timeLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		timeTextField = new JTextField();
		timeTextField.setText("0");
		timeTextField.setToolTipText("\u05D4\u05D2\u05D1\u05DC\u05EA \u05D6\u05DE\u05DF \u05E8\u05D9\u05E6\u05D4 \u05D1\u05E9\u05E0\u05D9\u05D5\u05EA.");
		timeTextField.setHorizontalAlignment(SwingConstants.CENTER);
		timeTextField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		timeTextField.setEnabled(false);
		timeTextField.setColumns(10);
		
		JButton patternButton = new JButton("\u05D4\u05D5\u05E8\u05D3 \u05EA\u05D1\u05E0\u05D9\u05EA...");
		patternButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		patternButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser(); 
				fc.setFileFilter(new FileNameExtensionFilter("Excel files", "xls"));
				int returnVal = fc.showSaveDialog(frame);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	            	File file = fc.getSelectedFile();
	            	if(!file.getName().endsWith(".xls")){
	            		file = new File(file.getAbsolutePath() + ".xls");
	            	}
	            	try{  
	            		Database db = new Database();
	            		db.writeTemplateFile(file);
				    }catch(IOException e1){
				    	Object[] option = { "\u05D0\u05D9\u05E9\u05D5\u05E8" };
						JOptionPane.showOptionDialog(frame,
								"����� �����, ��� ���� ����� ����� ��������.",
								"\u05E9\u05D2\u05D9\u05D0\u05D4", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null,
								option, option[0]);
				    }
	            }else{
	            	Object[] option = {"\u05D0\u05D9\u05E9\u05D5\u05E8"};
	            	JOptionPane.showOptionDialog(frame,
						    "\u05D4\u05E4\u05E2\u05D5\u05DC\u05D4 \u05D1\u05D5\u05D8\u05DC\u05D4 \u05E2\u05DC-\u05D9\u05D3\u05D9 \u05D4\u05DE\u05E9\u05EA\u05DE\u05E9.",
						    "\u05D1\u05D9\u05D8\u05D5\u05DC",
						    JOptionPane.OK_OPTION,
						    JOptionPane.CANCEL_OPTION,
						    null,
						    option,
						    option[0]);
	            }
			}
		});
		
		JButton preferButton = new JButton("\u05D1\u05D7\u05E8 \u05DE\u05E1\u05DC\u05D5\u05DC\u05D9\u05DD \u05DE\u05D5\u05E2\u05D3\u05E4\u05D9\u05DD...");
		preferButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		preferButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFrame.setVisible(true);
			}
		});

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(127)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(rateSlider, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 352, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(rateFormatted, 200, 200, 200)
											.addGap(58)
											.addComponent(rateLabel))
										.addComponent(yearSlider, GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(yearsFormatted, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
											.addComponent(yearsLabel))
										.addComponent(amountSlider, GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(amountFormatted, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
											.addGap(42)
											.addComponent(amountLabel)))
									.addGap(69))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(92)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(openFileCheckBox)
										.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(patternButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(chosenTextField, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(calcButton)
									.addGap(18)
									.addComponent(openFileButton)))
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(120)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(bulletCheckBox)
										.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
											.addComponent(yearCombCheckBox, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
											.addGroup(groupLayout.createSequentialGroup()
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
													.addComponent(bulletTextField, GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
													.addComponent(bulletComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(18)
												.addComponent(bulletAmountLabel))
											.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(preferButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(timeTextField, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
													.addGap(18)
													.addComponent(timeLabel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))))))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(bulletYearLabel)))))
					.addGap(78))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(108, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(amountFormatted, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(amountLabel))
					.addGap(18)
					.addComponent(amountSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(yearsFormatted, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(yearsLabel))
					.addGap(18)
					.addComponent(yearSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(17)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(rateFormatted, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(rateLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addComponent(rateSlider, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
					.addGap(29)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(bulletCheckBox)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(bulletYearLabel)
								.addComponent(bulletComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(bulletTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
								.addComponent(bulletAmountLabel))
							.addGap(15))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(openFileButton)
								.addComponent(calcButton, Alignment.TRAILING))
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(openFileCheckBox)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(chosenTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(patternButton))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(yearCombCheckBox)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(timeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(4)
									.addComponent(timeLabel, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))))
					.addGap(12)
					.addComponent(preferButton)
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
