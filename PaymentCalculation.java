package Mashkanta;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PaymentCalculation {

	public static enum PaymentType {
		SPITZER, BULLET, EQUAL_PRINCIPAL
	}

	public static enum LoanTrack {
		PRIME, DOLLAR, EURO, ONE_YEAR_MADAD_INDEXED_MORT, ONE_YEAR_MADAD_AGAH, TWO_YEAR_MADAD_INDEXED_MORT, TWO_YEAR_MADAD_AGAH, FIVE_YEAR_MADAD_INDEXED_MORT, FIVE_YEAR_MADAD_AGAH, FIVE_YEAR_NO_MADAD, CONST_MADAD, CONST
	}

	// Outputs
	List<MonthlyPayment> payment;
	Double totalPayment = 0.0;
	Double c;

	// User Inputs
	Double totalAmount;
	Integer months;
	PaymentType paymentType;
	LoanTrack paymentRoute;
	Double fixedInterest;
	Database db;

	// Constructors
	PaymentCalculation(Double totalAmount, Integer months, Double interest) {
		this.totalAmount = totalAmount;
		this.months = months;
		this.fixedInterest = interest;
		payment = new ArrayList<MonthlyPayment>(months);
		for (int i = 0; i < months; i++) {
			payment.add(new MonthlyPayment(i));
			
		}
	}

	PaymentCalculation(Database db, Double totalAmount, Integer months, PaymentType paymentType, LoanTrack paymentRoute,
			Double fixedInterest) throws IOException {
		this.db = db;
		this.totalAmount = totalAmount;
		this.months = months;
		this.paymentType = paymentType;
		this.paymentRoute = paymentRoute;
		this.fixedInterest = fixedInterest;
		payment = new ArrayList<MonthlyPayment>(months);
		for (int i = 0; i < months; i++) {
			payment.add(new MonthlyPayment(i));
		}
		if(months > 0) {
			Calculate();
		}else{
			this.c = 0.0;
		}
	}
	
	PaymentCalculation(Database db, Double totalAmount, Integer months, Integer i, Double fixedInterest) {
		this.totalAmount = totalAmount;
		this.months = months;
		this.fixedInterest = fixedInterest;
		this.db = db;
		switch(i){
		case(0):
			this.paymentRoute = LoanTrack.PRIME;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(1):
			this.paymentRoute = LoanTrack.PRIME;
			this.paymentType = PaymentType.EQUAL_PRINCIPAL;
			break;
		case(2):
			this.paymentRoute = LoanTrack.PRIME;
			this.paymentType = PaymentType.BULLET;
			break;
		case(3):
			this.paymentRoute = LoanTrack.DOLLAR;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(4):
			this.paymentRoute = LoanTrack.DOLLAR;
			this.paymentType = PaymentType.EQUAL_PRINCIPAL;
			break;
		case(5):
			this.paymentRoute = LoanTrack.DOLLAR;
			this.paymentType = PaymentType.BULLET;
			break;
		case(6):
			this.paymentRoute = LoanTrack.EURO;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(7):
			this.paymentRoute = LoanTrack.EURO;
			this.paymentType = PaymentType.EQUAL_PRINCIPAL;
			break;
		case(8):
			this.paymentRoute = LoanTrack.EURO;
			this.paymentType = PaymentType.BULLET;
			break;
		case(9):
			this.paymentRoute = LoanTrack.ONE_YEAR_MADAD_INDEXED_MORT;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(10):
			this.paymentRoute = LoanTrack.ONE_YEAR_MADAD_INDEXED_MORT;
			this.paymentType = PaymentType.BULLET;
			break;
		case(11):
			this.paymentRoute = LoanTrack.ONE_YEAR_MADAD_AGAH;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(12):
			this.paymentRoute = LoanTrack.ONE_YEAR_MADAD_AGAH;
			this.paymentType = PaymentType.BULLET;
			break;
		case(13):
			this.paymentRoute = LoanTrack.TWO_YEAR_MADAD_INDEXED_MORT;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(14):
			this.paymentRoute = LoanTrack.TWO_YEAR_MADAD_AGAH;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(15):
			this.paymentRoute = LoanTrack.FIVE_YEAR_MADAD_INDEXED_MORT;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(16):
			this.paymentRoute = LoanTrack.FIVE_YEAR_MADAD_AGAH;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(17):
			this.paymentRoute = LoanTrack.FIVE_YEAR_NO_MADAD;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(18):
			this.paymentRoute = LoanTrack.CONST_MADAD;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(19):
			this.paymentRoute = LoanTrack.CONST_MADAD;
			this.paymentType = PaymentType.EQUAL_PRINCIPAL;
			break;
		case(20):
			this.paymentRoute = LoanTrack.CONST_MADAD;
			this.paymentType = PaymentType.BULLET;
			break;
		case(21):
			this.paymentRoute = LoanTrack.CONST;
			this.paymentType = PaymentType.SPITZER;
			break;
		case(22):
			this.paymentRoute = LoanTrack.CONST;
			this.paymentType = PaymentType.EQUAL_PRINCIPAL;
			break;
		case(23):
			this.paymentRoute = LoanTrack.CONST;
			this.paymentType = PaymentType.BULLET;
			break;
		}
		payment = new ArrayList<MonthlyPayment>(months);
		for (int j = 0; j < months; j++) {
			payment.add(new MonthlyPayment(i));
		}
	}

	public void Calculate() throws IOException {
		List<Double> interest = new ArrayList<Double>();
		List<Double> CPI = new ArrayList<Double>();

		payment.get(0).setBalance(totalAmount);
		Iterator<MonthlyPayment> itr = payment.iterator();

		Integer k = 0;
		Double diff = totalAmount;
		MonthlyPayment monthly = null;
		switch (paymentRoute) {
		case PRIME:	
			interest = db.getPrime();
			for (int i = 0; i < months; i++) {
				CPI.add(0.0);
			}
			break;
		case DOLLAR:
			interest = db.getDollarLibor();
			CPI = db.getDollar();
			break;
		case EURO:
			interest = db.getEuroLibor();
			CPI = db.getEuro();
			break;
		case ONE_YEAR_MADAD_INDEXED_MORT:
			interest = db.getCPIIndexedMortgagesInterestOneYear();
			CPI = db.getCPI();
			break;
		case ONE_YEAR_MADAD_AGAH:
			interest = db.getIndexedAGAHOneYear();
			CPI = db.getCPI();
			break;
		case TWO_YEAR_MADAD_INDEXED_MORT:
			interest = db.getCPIIndexedMortgagesInterestTwoYear();
			CPI = db.getCPI();
			break;
		case TWO_YEAR_MADAD_AGAH:
			interest = db.getIndexedAGAHTwoYear();
			CPI = db.getCPI();
			break;
		case FIVE_YEAR_MADAD_INDEXED_MORT:
			interest = db.getCPIIndexedMortgagesInterestFiveYear();
			CPI = db.getCPI();
			break;
		case FIVE_YEAR_MADAD_AGAH:
			interest = db.getindexedAGAHFiveYearPred();
			CPI = db.getCPI();
			break;
		case FIVE_YEAR_NO_MADAD:
			interest = db.getAGAHNotIndexedFiveYearPred();
			for (int i = 0; i < months; i++) {
				CPI.add(0.0);
			}
			break;
		case CONST_MADAD:
			for (int i = 0; i < months; i++) {
				interest.add(fixedInterest);
			}
			CPI = db.getCPI();
			break;
		case CONST:
			for (int i = 0; i < months; i++) {
				interest.add(fixedInterest);
				CPI.add(0.0);
			}
			break;
		}

		switch (paymentType) {
		case BULLET:
			monthly = itr.next();
			monthly.setBalance(totalAmount);
			monthly.setMonthlyPrincipal(0.0);
			monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
			monthly.setMonthlyTotal(monthly.getMonthlyInterest());
			monthly.setNewBalance(totalAmount);
			totalPayment += monthly.getMonthlyTotal();
			Double lastBalance = totalAmount;
			k++;
			while (itr.hasNext()) {
				monthly = itr.next();
				monthly.setBalance(lastBalance * (1 + (CPI.get(k) - CPI.get(k-1))/100));
				monthly.setMonthlyPrincipal(0.0);
				monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
				monthly.setMonthlyTotal(monthly.getMonthlyInterest());
				monthly.setNewBalance(monthly.getBalance());
				totalPayment += monthly.getMonthlyTotal();
				lastBalance = monthly.getBalance();
				k++;
			}
			// now we will calculate last month's monthly total
			totalPayment -= monthly.getMonthlyTotal();
			monthly.setMonthlyPrincipal(lastBalance);
			monthly.setMonthlyTotal(monthly.getMonthlyPrincipal() + monthly.getMonthlyInterest());
			monthly.setNewBalance(0.0);
			totalPayment += monthly.getMonthlyTotal();
			break;
		case EQUAL_PRINCIPAL:
			monthly = itr.next();
			monthly.setBalance(diff);
			monthly.setMonthlyPrincipal(monthly.getBalance() / (months-k));
			monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
			monthly.setMonthlyTotal(monthly.getMonthlyPrincipal() + monthly.getMonthlyInterest());
			diff = monthly.getBalance() - monthly.getMonthlyPrincipal();
			monthly.setNewBalance(diff);
			totalPayment += monthly.getMonthlyTotal();
			k++;
			while (itr.hasNext()) {
				monthly = itr.next();
				monthly.setBalance(diff*(1 + (CPI.get(k) - CPI.get(k-1))/100));
				monthly.setMonthlyPrincipal(monthly.getBalance() / (months-k));
				monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
				monthly.setMonthlyTotal(monthly.getMonthlyPrincipal() + monthly.getMonthlyInterest());
				diff = monthly.getBalance() - monthly.getMonthlyPrincipal();
				monthly.setNewBalance(diff);
				totalPayment += monthly.getMonthlyTotal();
				k++;
			}
			break;
		case SPITZER:
			monthly = itr.next();
			monthly.setBalance(diff);
			monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
			monthly.setMonthlyTotal((monthly.getBalance() * (interest.get(k) / 100) * Math.pow(1 + (interest.get(k) / 100), months-k))
					/ (Math.pow(1 + (interest.get(k) / 100), months-k) - 1));
			monthly.setMonthlyPrincipal(monthly.getMonthlyTotal() - monthly.getMonthlyInterest());
			diff = monthly.getBalance() - monthly.getMonthlyPrincipal();
			monthly.setNewBalance(diff);
			totalPayment += monthly.getMonthlyTotal();
			k++;
			while (itr.hasNext()) {
				monthly = itr.next();
				monthly.setBalance(diff*(1 + (CPI.get(k) - CPI.get(k-1))/100));
				monthly.setMonthlyInterest((interest.get(k) / 100) * monthly.getBalance());
				monthly.setMonthlyTotal((monthly.getBalance() 
						* (interest.get(k) / 100) * Math.pow(1 + (interest.get(k) / 100), months-k))
						/ (Math.pow(1 + (interest.get(k) / 100), months-k) - 1));
				monthly.setMonthlyPrincipal(monthly.getMonthlyTotal() - monthly.getMonthlyInterest());
				diff = monthly.getBalance() - monthly.getMonthlyPrincipal();
				monthly.setNewBalance(diff);
				totalPayment += monthly.getMonthlyTotal();
				k++;
			}
			break;
		}
		this.c = this.totalPayment/this.totalAmount;
	}
	
	public Double getTotalAmount() {
		return this.totalAmount;
	}

	public Double getTotalPayment() {
		return this.totalPayment;
	}
	
	public Double getInterest() {
		return this.fixedInterest;
	}
	
	public Double getC(){
		return c;
	}

	public static void printHeader() {
		int i;
		System.out.println("\nAmortization Schedule for  Borrower");
		for (i = 0; i < 62; i++)
			System.out.print("-");
		System.out.format("\n%-8s%-12s%-10s%-10s%-10s%-12s", " ", "Old", "Monthly", "Interest", "Principle", "New",
				"Balance");
		System.out.format("\n%-8s%-12s%-10s%-10s%-10s%-12s\n\n", "Month", "Balance", "Payment", "Paid", "Paid",
				"Balance");
	}

	// Inputs:
	// i - month
	// p - old balance
	// mp - monthly payment
	// ip - interest paid
	// pp - principal paid
	// newbal - new balance                 
	public static void printSch(int i, double p, double mp, double ip, double pp, double newbal) {
		System.out.format("%-8d%-12.3f%-10.3f%-10.3f%-10.3f%-12.3f\n", i, p, mp, ip, pp, newbal);
	}
	
	public List<MonthlyPayment> getPayments(){
		return this.payment;
	}
	
	public PaymentCalculation add(PaymentCalculation pc){
		Iterator<MonthlyPayment> thisItr = payment.iterator(),
				itr = pc.payment.iterator();
		while(itr.hasNext() && thisItr.hasNext()){
			MonthlyPayment monthly = itr.next(), thisMonthly = thisItr.next();
			thisMonthly.add(monthly);
		}
		this.totalPayment+=pc.getTotalPayment();
		return this;
	}
	
	public String[][] toArray() {
		String [][] str = new String[months][6];
		Integer i=0;
		Iterator<MonthlyPayment> itr = payment.iterator();
		while(itr.hasNext()){
			MonthlyPayment monthly = itr.next();
			str[i] = monthly.toArray();
			i++;
		}
		return str;
	}
}
