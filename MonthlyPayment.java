package Mashkanta;

public class MonthlyPayment {

	private Integer month;
	private Double oldBalance;
	private Double monthlyPrincipal;
	private Double monthlyInterest;
	private Double monthlyTotal;
	private Double newBalance;
	
	MonthlyPayment(Integer month, Double balance, Double monthlyFund, Double  monthlyInterest, Double monthlyTotal, Double newBalance){
		this.month = month;
		this.oldBalance = balance;
		this.monthlyPrincipal = monthlyFund;
		this.monthlyInterest = monthlyInterest;
		this.monthlyTotal = monthlyTotal;
		this.newBalance = newBalance;
	}
	
	MonthlyPayment(Integer i){
		this.month = i;
		this.oldBalance = 0.0;
		this.monthlyPrincipal = 0.0;
		this.monthlyInterest = 0.0;
		this.monthlyTotal = 0.0;
		this.newBalance = 0.0;
	}
	
// get
	public Double getBalance(){
		return this.oldBalance;
	}
	
	public Double getMonthlyPrincipal(){
		return this.monthlyPrincipal;
	}
	
	public Double getMonthlyInterest(){
		return this.monthlyInterest;
	}
	
	public Double getMonthlyTotal(){
		return this.monthlyTotal;
	}
	
	public Integer getMonth(){
		return this.month;
	}
	
	public String[] toArray(){
		String[] str = new String[6];
		str[5] = String.format("%d", this.month);
		str[4] = String.format("%.2f", this.oldBalance);
		str[3] = String.format("%.2f", this.monthlyTotal);
		str[2] = String.format("%.2f", this.monthlyInterest);
		str[1] = String.format("%.2f", this.monthlyPrincipal);
		str[0] = String.format("%.2f", this.newBalance);
		return str;
	}
	
	public MonthlyPayment add(MonthlyPayment mp){
		this.oldBalance += mp.oldBalance;
		this.monthlyTotal += mp.monthlyTotal;
		this.monthlyInterest += mp.monthlyInterest;
		this.monthlyPrincipal += mp.monthlyPrincipal;
		this.newBalance += mp.newBalance;
		return this;
	}

// set
		public void setBalance(Double newBalance){
			this.oldBalance = newBalance;
		}
		
		public void setMonthlyPrincipal(Double newMonthlyFund){
			this.monthlyPrincipal = newMonthlyFund;
		}
		
		public void setMonthlyInterest(Double newMonthlyInterest){
			this.monthlyInterest = newMonthlyInterest;
		}
		
		public void setMonthlyTotal(Double newMonthlyTotal){
			this.monthlyTotal = newMonthlyTotal;
		}
		
		public void setMonth(Integer month){
			this.month = month;
		}
		
		public void setNewBalance(Double newBalance){
			this.newBalance = newBalance;
		}
}
