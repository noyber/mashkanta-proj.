package Mashkanta;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JEditorPane;

public class GuiResult extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTable mixTable;
	private JTextField monthlyRetText;
	private JTextField totalRetText;

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public GuiResult(LPResult res) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 939, 991);
		
		UIManager.put("ToolTip.font", new FontUIResource("Tahoma", Font.PLAIN, 14));

		contentPane = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
			    super.paintComponent(g); // paint the background image and scale it to fill the entire space
			    BufferedImage image = null;
				try {
					image = ImageIO.read(this.getClass().getResource("/m7.jpg"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    g.drawImage(image, 0, 0, null);
			}
		};
		setContentPane(new JScrollPane(contentPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		JScrollPane tableScrollPane = new JScrollPane();
		tableScrollPane.setOpaque(false);
		tableScrollPane.getViewport().setOpaque(false);

		JLabel label = new JLabel("\u05D8\u05D1\u05DC\u05EA \u05D4\u05D7\u05D6\u05E8\u05D9\u05DD");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JLabel monthlyRetlabel = new JLabel(
				"\u05D4\u05D7\u05D6\u05E8 \u05D7\u05D5\u05D3\u05E9\u05D9 \u05DE\u05DE\u05D5\u05E6\u05E2");
		monthlyRetlabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		monthlyRetlabel.setHorizontalAlignment(JLabel.CENTER);

		monthlyRetText = new JTextField();
		monthlyRetText.setEditable(false);
		monthlyRetText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		monthlyRetText.setColumns(10);
		monthlyRetText.setText(String.format("%.2f", res.getAvgPayment()));
		monthlyRetText.setHorizontalAlignment(JTextField.CENTER);

		JLabel totalRetLabel = new JLabel("\u05E1\u05DA \u05D4\u05DB\u05DC \u05D4\u05D7\u05D6\u05E8");
		totalRetLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		totalRetLabel.setHorizontalAlignment(JLabel.CENTER);

		totalRetText = new JTextField();
		totalRetText.setEditable(false);
		totalRetText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		totalRetText.setColumns(10);
		totalRetText.setText(String.format("%.2f", res.getTotalPayment()));
		totalRetText.setHorizontalAlignment(JTextField.CENTER);

		JButton saveButton = new JButton();
		saveButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		saveButton.setHorizontalTextPosition(SwingConstants.CENTER);
		saveButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		saveButton.setText("\u05E9\u05DE\u05D5\u05E8");
		ImageIcon saveIcon = new ImageIcon(this.getClass().getResource("/Save-52.png"));
		saveButton.setIcon(saveIcon);
		saveButton.setToolTipText(
				"\u05E9\u05DE\u05D5\u05E8 \u05E0\u05EA\u05D5\u05E0\u05D9\u05DD \u05D1\u05E7\u05D5\u05D1\u05E5 \u05D0\u05E7\u05E1\u05DC");
		saveButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser(); 
				fc.setFileFilter(new FileNameExtensionFilter("Excel files", "xls"));
	            int returnVal = fc.showSaveDialog(contentPane);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	            	File file = fc.getSelectedFile();
	            	if(!file.getName().endsWith(".xls")){
	            		file = new File(file.getAbsolutePath() + ".xls");
	            	}
	            	try {
						Database.exportTable(table, mixTable, file, String.format("%.2f", res.getTotalPayment()), String.format("%.2f", res.getTotalAmount()), String.format("%.2f", res.getInterest()*12));
					} catch (IOException e1) {
						Object[] option = {"\u05D0\u05D9\u05E9\u05D5\u05E8"};
						JOptionPane.showOptionDialog(contentPane,
								"\u05D4\u05E4\u05E2\u05D5\u05DC\u05D4 \u05E0\u05DB\u05E9\u05DC\u05D4, \u05E0\u05E1\u05D4 \u05E9\u05D5\u05D1.",
							    "\u05E9\u05D2\u05D9\u05D0\u05D4",
							    JOptionPane.OK_OPTION,
							    JOptionPane.ERROR_MESSAGE,
							    null,
							    option,
							    option[0]);
					}
	            }else{
	            	Object[] option = {"\u05D0\u05D9\u05E9\u05D5\u05E8"};
	            	JOptionPane.showOptionDialog(contentPane,
						    "\u05D4\u05E4\u05E2\u05D5\u05DC\u05D4 \u05D1\u05D5\u05D8\u05DC\u05D4 \u05E2\u05DC-\u05D9\u05D3\u05D9 \u05D4\u05DE\u05E9\u05EA\u05DE\u05E9.",
						    "\u05D1\u05D9\u05D8\u05D5\u05DC",
						    JOptionPane.OK_OPTION,
						    JOptionPane.CANCEL_OPTION,
						    null,
						    option,
						    option[0]);
	            }
			}
		});

		JPanel graphPanel = new JPanel();
		graphPanel.setOpaque(false);

		TimeSeries principalSeries = new TimeSeries("\u05E7\u05E8\u05DF");
		TimeSeries interestSeries = new TimeSeries("\u05E8\u05D9\u05D1\u05D9\u05EA");
		TimeSeries totalSeries = new TimeSeries("\u05E1\u05D4\"\u05DB \u05D4\u05D7\u05D6\u05E8");
		Month m = new Month();
		Iterator<MonthlyPayment> itr = res.getPc().getPayments().iterator();
		while (itr.hasNext()) {
			MonthlyPayment monthly = itr.next();
			principalSeries.add(m, monthly.getMonthlyPrincipal());
			interestSeries.add(m, monthly.getMonthlyInterest());
			totalSeries.add(m, monthly.getMonthlyTotal());
			m = (Month) m.next();
		}

		// Add the series to your data set
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(principalSeries);
		dataset.addSeries(interestSeries);
		dataset.addSeries(totalSeries);

		// Generate the graph
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"\u05D2\u05E8\u05E3 \u05EA\u05E9\u05DC\u05D5\u05DE\u05D9\u05DD \u05D7\u05D5\u05D3\u05E9\u05D9\u05D9\u05DD", // Title
				"\u05D7\u05D5\u05D3\u05E9", // x-axis Label
				"\u05EA\u05E9\u05DC\u05D5\u05DD", // y-axis Label
				dataset, // Dataset
				// PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs
		);

		chart.setBackgroundPaint(null);
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(null);
//		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(true);
			renderer.setBaseShapesFilled(true);
			renderer.setDrawSeriesLineAsPath(true);
		}

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MM-yyyy"));

		ChartPanel CP = new ChartPanel(chart);
		CP.setOpaque(false);
		CP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		graphPanel.add(CP, BorderLayout.CENTER);
		graphPanel.validate();
		
		JEditorPane jep = new JEditorPane("text/html", "");
		jep.setBackground(Color.WHITE);
		jep.setEditable(false);
		jep.setOpaque(false);
		jep.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
					openWebpage(hle.getURL());
				}
			}
		});
		String mixString = "<html><div WIDTH=850><font face=\"Tahoma\" size=\"5\"><p align=\"right\">";
		mixString = mixString.concat("<font size=\"6\"><b>����� ������� ������:</b></font>");
		double sol[] = res.getSol();
		
		DefaultTableModel model = new DefaultTableModel(new String[] { "����", "%", "���� ����", "�����" }, 0);
		if (sol[0] >= 0.0001 && res.getMonthsComb().get(0) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(0)/12 + " ����", String.format("%.2f", sol[0] * 100) + "%", "�����", "����� ����� ����� �� ���� ������," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27942/'>����� ����� ����� �� ���� ������</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[0] * 100, res.getMonthsComb().get(0)/12));
		}
		if (sol[1] >= 0.0001 && res.getMonthsComb().get(1) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(1)/12 + " ����", String.format("%.2f", sol[1] * 100) + "%", "��� ����", "����� ����� ����� �� ���� ������," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27942/'>����� ����� ����� �� ���� ������</a>" + String.format(", ���� ���� ��� ����, %.2f%%, %d ���� ", sol[1] * 100, res.getMonthsComb().get(1)/12));
		}
		if (sol[2] >= 0.0001 && res.getMonthsComb().get(2) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(2)/12 + " ����", String.format("%.2f", sol[2] * 100) + "%", "�����", "����� ����� ����� �� ���� ������," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27942/'>����� ����� ����� �� ���� ������</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[2] * 100, res.getMonthsComb().get(2)/12));
		}
		if (sol[3] >= 0.0001 && res.getMonthsComb().get(3) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(3)/12 + " ����", String.format("%.2f", sol[3] * 100) + "%", "�����", "����� ����� ����� �� ���� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27947/'>����� ����� ����� �� ���� ���� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[3] * 100, res.getMonthsComb().get(3)/12));
		}
		if (sol[4] >= 0.0001 && res.getMonthsComb().get(4) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(4)/12 + " ����", String.format("%.2f", sol[4] * 100) + "%", "��� ����", "����� ����� ����� �� ���� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27947/'>����� ����� ����� �� ���� ���� ����</a>" + String.format(", ���� ���� ��� ����, %.2f%%, %d ���� ", sol[4] * 100, res.getMonthsComb().get(4)/12));
		}
		if (sol[5] >= 0.0001 && res.getMonthsComb().get(5) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(5)/12 + " ����", String.format("%.2f", sol[5] * 100) + "%", "�����", "����� ����� ����� �� ���� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27947/'>����� ����� ����� �� ���� ���� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[5] * 100, res.getMonthsComb().get(5)/12));
		}
		if (sol[6] >= 0.0001 && res.getMonthsComb().get(6) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(6)/12 + " ����", String.format("%.2f", sol[6] * 100) + "%", "�����", "����� ����� ����� �� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27949/'>����� ����� ����� �� ���� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[6] * 100, res.getMonthsComb().get(6)/12));
		}
		if (sol[7] >= 0.0001 && res.getMonthsComb().get(7) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(7)/12 + " ����", String.format("%.2f", sol[7] * 100) + "%", "��� ����", "����� ����� ����� �� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27949/'>����� ����� ����� �� ���� ����</a>" + String.format(", ���� ���� ��� ����, %.2f%%, %d ���� ", sol[7] * 100, res.getMonthsComb().get(7)/12));
		}
		if (sol[8] >= 0.0001 && res.getMonthsComb().get(8) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(8)/12 + " ����", String.format("%.2f", sol[8] * 100) + "%", "�����", "����� ����� ����� �� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27949/'>����� ����� ����� �� ���� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[8] * 100, res.getMonthsComb().get(8)/12));
		}
		if (sol[9] >= 0.0001 && res.getMonthsComb().get(9) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(9)/12 + " ����", String.format("%.2f", sol[9] * 100) + "%", "�����", "����� ����� ����� �� ��� ����� ��� (���� �������� ������ ���)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27956/'>����� ����� ����� �� ��� ����� ��� (���� �������� ������ ���)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[9] * 100, res.getMonthsComb().get(9)/12));
		}
		if (sol[10] >= 0.0001 && res.getMonthsComb().get(10) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(10)/12 + " ����", String.format("%.2f", sol[10] * 100) + "%", "�����", "����� ����� ����� �� ��� ����� ��� (���� �������� ������ ���)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27956/'>����� ����� ����� �� ��� ����� ��� (���� �������� ������ ���)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[10] * 100, res.getMonthsComb().get(10)/12));
		}
		if (sol[11] >= 0.0001 && res.getMonthsComb().get(11) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(11)/12 + " ����", String.format("%.2f", sol[11] * 100) + "%", "�����", "����� ����� ����� �� ��� ����� ��� (���� ��� ����)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27956/'>����� ����� ����� �� ��� ����� ��� (���� ��� ����)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[11] * 100, res.getMonthsComb().get(11)/12));
		}
		if (sol[12] >= 0.0001 && res.getMonthsComb().get(12) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(12)/12 + " ����", String.format("%.2f", sol[12] * 100) + "%", "�����", "����� ����� ����� �� ��� ����� ��� (���� ��� ����)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27956/'>����� ����� ����� �� ��� ����� ��� (���� ��� ����)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[12] * 100, res.getMonthsComb().get(12)/12));
		}
		if (sol[13] >= 0.0001 && res.getMonthsComb().get(13) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(13)/12 + " ����", String.format("%.2f", sol[13] * 100) + "%", "�����", "����� ����� ����� �� ������ ����� ��� (���� �������� ������ ���)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27963/'>����� ����� ����� �� ������ ����� ��� (���� �������� ������ ���)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[13] * 100, res.getMonthsComb().get(13)/12));
		}
		if (sol[14] >= 0.0001 && res.getMonthsComb().get(14) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(14)/12 + " ����", String.format("%.2f", sol[14] * 100) + "%", "�����", "����� ����� ����� �� ������ ����� ��� (���� ��� ����)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27963/'>����� ����� ����� �� ������ ����� ��� (���� ��� ����)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[14] * 100, res.getMonthsComb().get(14)/12));
		}
		if (sol[15] >= 0.0001 && res.getMonthsComb().get(15) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(15)/12 + " ����", String.format("%.2f", sol[15] * 100) + "%", "�����", "����� ����� ����� �� 5 ���� ����� ��� (���� �������� ������ ���)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/31495/'>����� ����� ����� �� ��� ���� ����� ��� (���� �������� ������ ���)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[15] * 100, res.getMonthsComb().get(15)/12));
		}
		if (sol[16] >= 0.0001 && res.getMonthsComb().get(16) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(16)/12 + " ����", String.format("%.2f", sol[16] * 100) + "%", "�����", "����� ����� ����� �� 5 ���� ����� ��� (���� ��� ����)," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/31495/'>����� ����� ����� �� ��� ���� ����� ��� (���� ��� ����)</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[16] * 100, res.getMonthsComb().get(16)/12));
		}
		if (sol[17] >= 0.0001 && res.getMonthsComb().get(17) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(17)/12 + " ����", String.format("%.2f", sol[17] * 100) + "%", "�����", "����� ����� ����� �� 5 ����, �� ���� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27969/'>����� ����� ����� �� ��� ����, �� ���� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[17] * 100, res.getMonthsComb().get(17)/12));
		}
		if (sol[18] >= 0.0001 && res.getMonthsComb().get(18) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(18)/12 + " ����", String.format("%.2f", sol[18] * 100) + "%", "�����", "����� ����� ����� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27994/'>����� ����� ����� ����� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[18] * 100, res.getMonthsComb().get(18)/12));
		}
		if (sol[19] >= 0.0001 && res.getMonthsComb().get(19) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(19)/12 + " ����", String.format("%.2f", sol[19] * 100) + "%", "��� ����", "����� ����� ����� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27994/'>����� ����� ����� ����� ����</a>" + String.format(", ���� ���� ��� ����, %.2f%%, %d ���� ", sol[19] * 100, res.getMonthsComb().get(19)/12));
		}
		if (sol[20] >= 0.0001 && res.getMonthsComb().get(20) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(20)/12 + " ����", String.format("%.2f", sol[20] * 100) + "%", "�����", "����� ����� ����� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/27994/'><����� ����� ����� ����� ����/a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[20] * 100, res.getMonthsComb().get(20)/12));
		}
		if (sol[21] >= 0.0001 && res.getMonthsComb().get(21) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(21)/12 + " ����", String.format("%.2f", sol[21] * 100) + "%", "�����", "����� ����� ����� �� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/28000/'>����� ����� ����� �� ����� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[21] * 100, res.getMonthsComb().get(21)/12));
		}
		if (sol[22] >= 0.0001 && res.getMonthsComb().get(22) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(22)/12 + " ����", String.format("%.2f", sol[22] * 100) + "%", "��� ����", "����� ����� ����� �� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/28000/'>����� ����� ����� �� ����� ����</a>" + String.format(", ���� ���� ��� ����, %.2f%%, %d ���� ", sol[22] * 100, res.getMonthsComb().get(22)/12));
		}
		if (sol[23] >= 0.0001 && res.getMonthsComb().get(23) > 0){
			model.addRow(new Object[] { res.getMonthsComb().get(23)/12 + " ����", String.format("%.2f", sol[23] * 100) + "%", "�����", "����� ����� ����� �� ����� ����," });
			mixString = mixString.concat("<br><a href='http://www.blms.co.il/Articles/28000/'>����� ����� ����� �� ����� ����</a>" + String.format(", ���� ���� �����, %.2f%%, %d ���� ", sol[23] * 100, res.getMonthsComb().get(23)/12));
		}
		mixTable = new JTable(model);
		jep.setText(mixString);
		
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		rightRenderer.setOpaque(false);
		tableScrollPane.setViewportView(table);
		String[] col = new String[] { "\u05D9\u05EA\u05E8\u05D4 \u05D7\u05D3\u05E9\u05D4",
				"\u05EA\u05E9\u05DC\u05D5\u05DD \u05E7\u05E8\u05DF",
				"\u05EA\u05E9\u05DC\u05D5\u05DD \u05E8\u05D9\u05D1\u05D9\u05EA",
				"\u05EA\u05E9\u05DC\u05D5\u05DD \u05D7\u05D5\u05D3\u05E9\u05D9",
				"\u05D9\u05EA\u05E8\u05D4 \u05D9\u05E9\u05E0\u05D4", "\u05D7\u05D5\u05D3\u05E9" };
		DefaultTableModel tableModel = new DefaultTableModel(res.getPc().toArray(), col);
		table = new JTable(tableModel);
		table.setOpaque(false);
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.getColumnModel().getColumn(0).setPreferredWidth(110);
		table.getColumnModel().getColumn(1).setPreferredWidth(110);
		table.getColumnModel().getColumn(2).setPreferredWidth(110);
		table.getColumnModel().getColumn(3).setPreferredWidth(110);
		table.getColumnModel().getColumn(4).setPreferredWidth(110);
		table.getColumnModel().getColumn(5).setPreferredWidth(40);
		table.getColumnModel().getColumn(0).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
		tableScrollPane.setViewportView(table);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(196)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(totalRetLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(totalRetText, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 260, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(monthlyRetlabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(monthlyRetText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(155))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(782, Short.MAX_VALUE)
					.addComponent(label)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(jep, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE)
						.addComponent(tableScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE)
						.addComponent(saveButton, Alignment.LEADING)
						.addComponent(graphPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(saveButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jep, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(totalRetLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(totalRetText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(monthlyRetlabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(monthlyRetText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(25)
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tableScrollPane, GroupLayout.PREFERRED_SIZE, 326, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(graphPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(34))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}

	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	}
}
