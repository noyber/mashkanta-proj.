package Mashkanta;

import java.util.ArrayList;
import java.util.Iterator;
import org.apache.log4j.BasicConfigurator;
import com.joptimizer.optimizers.LPOptimizationRequest;
import com.joptimizer.optimizers.LPPrimalDualMethod;
import Mashkanta.PaymentCalculation.LoanTrack;
import Mashkanta.PaymentCalculation.PaymentType;



public class LPAlgorithm {
	private LPResult bestRes;
	private Double totalAmount; 
	private Double fixedInterest;
	private Integer months;
	private Double bulletAmount; 
	private Integer bulletMonths;
	private Database db;
	private Double prefered[];
	
	public LPAlgorithm(Double totalAmount, Integer months, Double fixedInterest, Database db, Double prefered[], Double bulletAmount, Integer bulletMonths, boolean doCombCalc, Integer timeout) throws Exception {
		this.totalAmount = totalAmount;
		this.months = months;
		this.fixedInterest = fixedInterest;
		this.db = db;
		this.bulletAmount = bulletAmount;
		this.bulletMonths = bulletMonths;
		this.prefered = prefered;
		bestRes = new LPResult();
		
		ArrayList<Integer> comb = new ArrayList<>();
		if(doCombCalc){
			comb.add(months);
			comb.add(months-12*2);
			comb.add(months+12*2);
			createCombinationArray(System.currentTimeMillis(), timeout, comb, 17, new ArrayList<Integer>());
		}else{
			for(int i=0; i<17; i++){
				comb.add(months);
			}
			LPcalc(comb);
		}
	}
	
	public void LPcalc(ArrayList<Integer> monthsArrayNoBullet) throws Exception {

		// Changing monthly:
		// 0. adjustable interest based on prime (SPITZER)
		// 1. adjustable interest based on prime (EQUAL_PRINCIPAL)
		// 2. adjustable interest based on prime (BULLET)
		// Changing 6-monthly:
		// 3. adjustable interest based on dollar (SPITZER)
		// 4. adjustable interest based on dollar (EQUAL_PRINCIPAL)
		// 5. adjustable interest based on dollar (BULLET)
		// 6. adjustable interest based on euro (SPITZER)
		// 7. adjustable interest based on euro (EQUAL_PRINCIPAL)
		// 8. adjustable interest based on euro (BULLET)
		// 9. yearly adjustable interest CPI-indexed (based on average rate of interest) (SPITZER)
		// 10. yearly adjustable interest CPI-indexed (based on average rate of interest) (BULLET)
		// 11. yearly adjustable interest CPI-indexed (based on governmental bonds) (SPITZER)
		// 12. yearly adjustable interest CPI-indexed (based on governmental bonds) (BULLET)
		// 13. 2 yearly adjustable interest CPI-indexed (based on average rate of interest( (SPITZER)
		// 14. 2 yearly adjustable interest CPI-indexed (based on governmental bonds) (SPITZER)
		// Fixed rate:
		// 15. 5 yearly adjustable interest CPI-indexed (based on average rate of interest) (SPITZER)
		// 16. 5 yearly adjustable interest CPI-indexed (based on governmental bonds) (SPITZER)
		// 17. 5 yearly adjustable interest not CPI-indexed (SPITZER)
		// 18. constant interest based on CPI (SPITZER)
		// 19. constant interest based on CPI (EQUAL_PRINCIPAL)
		// 20. constant interest based on CPI (BULLET)
		// 21. constant interest not based on CPI (SPITZER)
		// 22. constant interest not based on CPI (EQUAL_PRINCIPAL)
		// 23. constant interest not based on CPI (BULLET)
		
		
		BasicConfigurator.configure();
		ArrayList<Integer> monthsArray = new ArrayList<Integer>();
		Iterator<Integer> itr = monthsArrayNoBullet.iterator();
		for(int i=0; i<24; i++){
			if(i == 2 || i == 5 || i == 8 || i == 10 || i == 12 || i == 20 || i == 23){
				monthsArray.add(bulletMonths);
			}else{
				monthsArray.add(itr.next());
			}
		}
		
		// Objective function
		double[] c = new double[] { new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(0), PaymentType.SPITZER, LoanTrack.PRIME, fixedInterest).getC(), 
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(1), PaymentType.EQUAL_PRINCIPAL, LoanTrack.PRIME, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(2), PaymentType.BULLET, LoanTrack.PRIME, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(3), PaymentType.SPITZER, LoanTrack.DOLLAR, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(4), PaymentType.EQUAL_PRINCIPAL, LoanTrack.DOLLAR, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(5), PaymentType.BULLET, LoanTrack.DOLLAR, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(6), PaymentType.SPITZER, LoanTrack.EURO, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(7), PaymentType.EQUAL_PRINCIPAL, LoanTrack.EURO, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(8), PaymentType.BULLET, LoanTrack.EURO, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(9), PaymentType.SPITZER, LoanTrack.ONE_YEAR_MADAD_INDEXED_MORT, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(10), PaymentType.BULLET, LoanTrack.ONE_YEAR_MADAD_INDEXED_MORT, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(11), PaymentType.SPITZER, LoanTrack.ONE_YEAR_MADAD_AGAH, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(12), PaymentType.BULLET, LoanTrack.ONE_YEAR_MADAD_AGAH, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(13), PaymentType.SPITZER, LoanTrack.TWO_YEAR_MADAD_INDEXED_MORT, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(14), PaymentType.SPITZER, LoanTrack.TWO_YEAR_MADAD_AGAH, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(15), PaymentType.SPITZER, LoanTrack.FIVE_YEAR_MADAD_INDEXED_MORT, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(16), PaymentType.SPITZER, LoanTrack.FIVE_YEAR_MADAD_AGAH, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(17), PaymentType.SPITZER, LoanTrack.FIVE_YEAR_NO_MADAD, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(18), PaymentType.SPITZER, LoanTrack.CONST_MADAD, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(19), PaymentType.EQUAL_PRINCIPAL, LoanTrack.CONST_MADAD, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(20), PaymentType.BULLET, LoanTrack.CONST_MADAD, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(21), PaymentType.SPITZER, LoanTrack.CONST, fixedInterest).getC(),
				new PaymentCalculation(db, totalAmount-bulletAmount, monthsArray.get(22), PaymentType.EQUAL_PRINCIPAL, LoanTrack.CONST, fixedInterest).getC(),
				new PaymentCalculation(db, bulletAmount, monthsArray.get(23), PaymentType.BULLET, LoanTrack.CONST, fixedInterest).getC()};
		
		
		// Inequalities constraints
		// 0. Restrictions of BOI - the adjustable part is at most 1/3 of the total amount.
		// 1. Restrictions of BOI - the fixed part is at least 2/3 of the total amount.
		double[][] G = new double[][] {
				{ 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
				{ 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -1., -1., -1., -1., -1., -1., -1., -1., -1. } };
		double[] h = new double[] { 1. / 3., -2. / 3. };
		
		// equalities constraints
		double[][] A;
		double[] b;
		A = new double[][] {
			{ prefered[0], prefered[1], 0., prefered[3], prefered[4], 0., prefered[6], prefered[7], 0., prefered[9], 0., prefered[11], 0., prefered[13], prefered[14], prefered[15], prefered[16], prefered[17], prefered[18], prefered[19], 0., prefered[21], prefered[22], 0. },
			{ 0., 0., prefered[2], 0., 0., prefered[5], 0., 0., prefered[8], 0., prefered[10], 0., prefered[12], 0., 0., 0., 0., 0., 0., 0., prefered[20], 0., 0., prefered[23] } };
		b = new double[] { (totalAmount-bulletAmount)/totalAmount, bulletAmount/totalAmount };

		// Bounds on variables
		// All parts are at least 0.
		// The adjustable part is maximum 1/3 of the loan.
		// The fixed part is maximum 1 of the loan.
		double[] lb = new double[] { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };
		double[] ub = new double[] { 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1. / 3., 1., 1., 1., 1., 1., 1., 1., 1., 1. };

		// optimization problem
		LPOptimizationRequest or = new LPOptimizationRequest();
		or.setC(c);
		or.setG(G);
		or.setH(h);
		or.setA(A);
		or.setB(b);
		or.setLb(lb);
		or.setUb(ub);
		or.setDumpProblem(false);
		
		// optimization
		LPPrimalDualMethod opt = new LPPrimalDualMethod();
		opt.setLPOptimizationRequest(or);
		opt.optimize();
		double[] sol = opt.getOptimizationResponse().getSolution();
		
		// Calculate each monthly payment as sum of the chosen tracks.
		PaymentCalculation totalPc = new PaymentCalculation(totalAmount, months, fixedInterest);
		for(int i=0; i<24; i++){
			if(sol[i] > 0.01){
				Integer m;
				if(i == 2 || i ==5 || i == 8 || i == 10 || i==12 || i == 20 || i == 23){
					m = bulletMonths;
				}else{
					m = months;
				}
				if(m != 0){
					PaymentCalculation pcPerTrack = new PaymentCalculation(db, totalAmount*sol[i], m, i, fixedInterest);
					pcPerTrack.Calculate();
					totalPc.add(pcPerTrack);
				}
			}
		}

		bestRes.swapIfBetter(new LPResult(monthsArray, sol, totalPc, totalAmount, fixedInterest));
	}
	
	
	public LPResult getBestResult(){
		return this.bestRes;
	}
	
	void createCombinationArray(Long startTime, Integer lim, ArrayList<Integer> numbers, int n, ArrayList<Integer> start) throws Exception {
		if(System.currentTimeMillis() - startTime > lim){
			return;
		}
		if (start.size() >= n) {
			LPcalc(start);
		} else {
			for (Integer x : numbers) {
				start.add(x);
				createCombinationArray(startTime, lim, numbers, n, start);
				start.remove(start.lastIndexOf(x));
			}
		}
	}
}
